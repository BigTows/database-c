#ifndef PERSON_H
#define PERSON_H

#include <string>
class Person
{
private:
std::string firstName;
std::string secondName;

public:
    Person(const std::string  * firstName, const std::string * secondName);
    std::string getFirstName() const;
    void setFirstName(const std::string &value);
    std::string getSecondName() const;
    void setSecondName(const std::string &value);
};

#endif // PERSON_H
