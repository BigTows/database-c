#include "customerpackageentity.h"



Person *CustomerPackageEntity::getCustomer() const
{
    return customer;
}

void CustomerPackageEntity::setCustomer(Person *value)
{
    customer = value;
}

Person *CustomerPackageEntity::getSender() const
{
    return sender;
}

void CustomerPackageEntity::setSender(Person *value)
{
    sender = value;
}

double CustomerPackageEntity::getCost() const
{
    return cost;
}

void CustomerPackageEntity::setCost(double value)
{
    cost = value;
}

double CustomerPackageEntity::getWeight() const
{
    return weight;
}

void CustomerPackageEntity::setWeight(double value)
{
    weight = value;
}

CustomerPackageEntity * CustomerPackageEntity::factory(QStringList CSV)
{
    std::string  firstName= CSV.at(0).toStdString();
    std::string  secondName= CSV.at(1).toStdString();
    Person * customer = new Person(&firstName,&secondName);
    firstName= CSV.at(2).toStdString();
    secondName= CSV.at(3).toStdString();
    Person * sender  = new Person(&firstName,&secondName);
    QDateTime * dateTime = new QDateTime(QDateTime::fromString(CSV.at(4), "dd.MM.yyyy/HH:mm"));
    float weightCSV = CSV.at(5).toFloat();
    float costCSV = CSV.at(6).toFloat();
    CustomerPackageEntity * entity = new CustomerPackageEntity();
    entity->setCost(costCSV);
    entity->setWeight(weightCSV);
    entity->setDateTime(dateTime);
    entity->setSender(sender);
    entity->setCustomer(customer);
    return entity;
}

void  CustomerPackageEntity::printData()
{
    MessageHelper::log("Customer: "+customer->getFirstName()+" "+customer->getSecondName());
    MessageHelper::log("Sender: "+sender->getFirstName()+" "+sender->getSecondName());
    MessageHelper::log("Date and Time: "+dateTime->toString().toStdString());
    MessageHelper::log("Cost and Weight: "+std::to_string(cost)+" "+std::to_string(weight));
}

//CustomerPackageEntity::CustomerPackageEntity(Person & sender,Person & customer,QDateTime &time,float& w,float &c):sender(sender),customer(customer),dateTime(dateTime),weight(w),cost(c)
//{}

CustomerPackageEntity::CustomerPackageEntity()
{
 CustomerPackageEntity::cost = 0;
 CustomerPackageEntity::weight = 0;
}

QDateTime *CustomerPackageEntity::getDateTime() const
{
    return dateTime;
}

void CustomerPackageEntity::setDateTime(QDateTime *value)
{
    dateTime = value;
}
