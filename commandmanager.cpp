#include "commandmanager.h"

CommandManager::CommandManager() = default;

void CommandManager::addCommand(std::string name, std::string description,std::function<int()> callback) {
    commands.push_back(*new Command(name,description,callback));
};

void CommandManager::viewCommands() {
    MessageHelper::log(LINE);
    for (auto &command : commands)
        MessageHelper::log(command.getNameCommand() + " - " + command.getDescriptionCommand());
    MessageHelper::log(LINE);
}

bool CommandManager::isSet(std::string &nameCommand) {
    for (auto &command : commands) {
        if (command.getNameCommand() == nameCommand) {
            return true;
        }
    }
    return false;
}

Command * CommandManager::getCommandByName(std::string &nameCommand) {
    for (auto &command : commands) {
        if (command.getNameCommand() == nameCommand) {
            return &command;
        }
    }
    return {};
}
