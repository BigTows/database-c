#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

#include <vector>
#include "messagehelper.h"
#include "command.h"
class CommandManager
{
    const std::string LINE = "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
    public:
        CommandManager();
        void addCommand(std::string,std::string,std::function<int()>);
        void viewCommands();
        bool isSet(std::string &name);
        Command * getCommandByName(std::string &name);
    private:
            std::vector<Command> commands;
};

#endif // COMMANDMANAGER_H
