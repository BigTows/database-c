#ifndef DATABASE_H
#define DATABASE_H

#include <QFile>
#include <QTextStream>
#include <messagehelper.h>
#include <vector>
#include "customerpackageentity.h"
class DataBase
{
public:
    DataBase();
     std::vector<CustomerPackageEntity> * getStorage();
     void addEntity(CustomerPackageEntity entity);
     void save();
     void deleteById(int id);
     CustomerPackageEntity getById(int id);
private:
    void loadFromFile();
    std::vector<CustomerPackageEntity> storage;
};

#endif // DATABASE_H
