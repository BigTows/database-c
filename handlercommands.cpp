#include "handlercommands.h"


void HandlerCommands::hookStream() {
    while (true){
    bool undefinedCommandFlag = false;
    std::string command;
    do{
        if (undefinedCommandFlag){
            MessageHelper::log("Undefined command '"+command+"'");
        }
        std::cin >> command;
        undefinedCommandFlag= true;
    } while(!(*commandManager).isSet(command));
    (*commandManager).getCommandByName(command)->getCallback()();
    }
}

CommandManager *HandlerCommands::getCommandManager() const
{
    return commandManager;
}

HandlerCommands::HandlerCommands(CommandManager *manager) {
    commandManager = manager;
}

