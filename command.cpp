#include "command.h"

std::function<int ()> Command::getCallback() const
{
    return callback;
}

const std::string &Command::getNameCommand() const {
    return nameCommand;
}

void Command::setNameCommand(const std::string &nameCommand) {
    Command::nameCommand = nameCommand;
}

const std::string &Command::getDescriptionCommand() const {
    return descriptionCommand;
}

void Command::setDescriptionCommand(const std::string &descriptionCommand) {
    Command::descriptionCommand = descriptionCommand;
}

Command::Command(const std::string &nameCommand, const std::string &descriptionCommand,std::function<int ()> &callback) : nameCommand(nameCommand),
                                                                                          descriptionCommand(
                                                                                                  descriptionCommand),
                                                                                          callback(callback)
{}

