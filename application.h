#ifndef APPLICATION_H
#define APPLICATION_H

#include "commandmanager.h"
#include "handlercommands.h"
#include "database.h"
#include "model.h"
#include "customerpackageentity.h"
class Application
{
private:
    static DataBase * dataBase;
    static CommandManager* initCommands();


public:
    static void run();

    static HandlerCommands *handlerCommands;
};

#endif // APPLICATION_H
