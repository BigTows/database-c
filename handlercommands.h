#ifndef HANDLERCOMMAND_H
#define HANDLERCOMMAND_H

#include "commandmanager.h"
class HandlerCommands
{
private:
    CommandManager *commandManager;

public:
    explicit HandlerCommands(CommandManager *manager);
    void hookStream();

    CommandManager *getCommandManager() const;
};

#endif // HANDLERCOMMAND_H
