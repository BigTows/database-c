#include "person.h"

std::string Person::getFirstName() const
{
    return firstName;
}

void Person::setFirstName(const std::string &value)
{
    firstName = value;
}

std::string Person::getSecondName() const
{
    return secondName;
}

void Person::setSecondName(const std::string &value)
{
    secondName = value;
}

Person::Person(const std::string * firstName,const std::string * secondName):firstName(*firstName),
    secondName(*secondName)
{

}
