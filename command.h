#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include <functional>
class Command
{
private:
    /**
     * @var  Store name command like "view"
     */
    std::string nameCommand;
    /**
     * @var  Store description about command
     */
    std::string descriptionCommand;
    std::function<int()> callback;
public:
    Command(const std::string &nameCommand, const std::string &descriptionCommand,std::function<int()>&callback);

    /**
     * Getter name command
     * @return name Command
     */
    const std::string &getNameCommand() const;

    /**
     * Setter name command
     * @param nameCommand
     */
    void setNameCommand(const std::string &nameCommand);

    /**
     * Getter description
     * @return description
     */
    const std::string &getDescriptionCommand() const;

    /**
     * Setter description
     * @param descriptionCommand
     */
    void setDescriptionCommand(const std::string &descriptionCommand);

    /**
     * @brief getCallback
     * @return
     */
    std::function<int ()> getCallback() const;

};

#endif // COMMAND_H
