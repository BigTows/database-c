#include "application.h"

HandlerCommands * Application::handlerCommands;
DataBase * Application::dataBase;

CommandManager* Application::initCommands()
{
    CommandManager * command = new CommandManager();

    command->addCommand("view","view all records",[] {
         MessageHelper::log("===========================");
         int id = 1;
        for (CustomerPackageEntity &entity : *dataBase->getStorage()) {
            MessageHelper::log("Id: "+std::to_string(id));
            entity.printData();
            id++;
             MessageHelper::log("===========================");
        }
        return 0;
    });
    command->addCommand("delete","delete one record",[] {
        unsigned int deletedItem = Model::getInt();
        if (deletedItem<=dataBase->getStorage()->size()){
            dataBase->deleteById(deletedItem);
            MessageHelper::log("Deleted by id: "+std::to_string(deletedItem));
        }else{
            MessageHelper::log("Id must be between 0 and "+std::to_string(dataBase->getStorage()->size()-1));
        }
        return 0;
    });
    command->addCommand("insert","insert one record",[] {
        CustomerPackageEntity entity;
        MessageHelper::log("Customer insert: ");
        entity.setCustomer(Model::getPersonByConsole());
        MessageHelper::log("Sender insert: ");
        entity.setSender(Model::getPersonByConsole());
        MessageHelper::log("Date and Time insert");
        entity.setDateTime(Model::getDate());
        MessageHelper::log("Weight insert");
        entity.setWeight(Model::getFloat());
        MessageHelper::log("Cost Insert");
        entity.setCost(Model::getFloat());
        dataBase->addEntity(entity);
        return 0;
    });
    command->addCommand("edit","edit 1 record",[]{
         unsigned int changeItem = Model::getInt();
        if (changeItem<=dataBase->getStorage()->size()){
            CustomerPackageEntity entity = dataBase->getById(changeItem);
            MessageHelper::log("Your pick");
            entity.printData();
            switch(Model::getInt()){
            case 1:{
                MessageHelper::log("Customer Insert");
                entity.setCustomer(Model::getPersonByConsole());
                break;
            }
            case 2:{
                MessageHelper::log("Sender Insert");
                entity.setSender(Model::getPersonByConsole());
                break;
            }
            case 3:{
                MessageHelper::log("Date Insert");
                entity.setDateTime(Model::getDate());
                break;
            }
            case 4:{
                MessageHelper::log("Insert cost");
                entity.setCost(Model::getFloat());
                break;
            }
            case 5:{
                MessageHelper::log("Insert weight");
                entity.setWeight(Model::getFloat());
                break;
            }
            default:{
                MessageHelper::log("Undefined property id");
            }

            }
            dataBase->deleteById(changeItem);
            dataBase->addEntity(entity);
        }else{
            MessageHelper::log("Id must be between 0 and "+std::to_string(dataBase->getStorage()->size()-1));
        }
        return 0;
    });
    command->addCommand("help","view all commands",[] {
        Application::handlerCommands->getCommandManager()->viewCommands();
               MessageHelper::printLines(12);
        return 0;
    });

    command->addCommand("save","save into file",[]{
        dataBase->save();
        return 0;
    });
    command->addCommand("exit","close application",[] {
        return 0;
    });
    return command;
}

void Application::run() {
    MessageHelper::printLines(2);
    dataBase = new DataBase();
    handlerCommands = new HandlerCommands(Application::initCommands());
    handlerCommands->getCommandManager()->viewCommands();
    MessageHelper::printLines(14);
    handlerCommands->hookStream();
}
