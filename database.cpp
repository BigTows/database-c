#include "database.h"

DataBase::DataBase()
{
    DataBase::loadFromFile();
}


void DataBase::loadFromFile()
{

    QFile file("/Users/bigtows/Projects/DataBase/db.csv");
    if(!file.open(QIODevice::ReadOnly)) {
        MessageHelper::log("Error on read file");
        return;
    }

    QTextStream in(&file);
    while(!in.atEnd()){
    QRegExp tagExp(";");
    QString line =  in.readLine();
    QStringList list = line.split(tagExp);
       storage.push_back(*CustomerPackageEntity::factory(list));
    }

}

std::vector<CustomerPackageEntity> * DataBase::getStorage()
{
    return &storage;
}

void DataBase::addEntity(CustomerPackageEntity entity)
{
    storage.insert(storage.begin(),entity);
}

void DataBase::save()
{
    QFile file("/Users/bigtows/Projects/DataBase/db.csv");
    if(!file.open(QIODevice::WriteOnly)) {
        MessageHelper::log("Error on read file");
        return;
    }
    QTextStream in(&file);

    for (unsigned int i=0;i<storage.size();i++){
        in << QString::fromStdString(storage.at(i).getCustomer()->getFirstName()) <<";";
        in << QString::fromStdString(storage.at(i).getCustomer()->getSecondName())<<";";
        in << QString::fromStdString(storage.at(i).getSender()->getFirstName())<<";";
        in << QString::fromStdString(storage.at(i).getSender()->getSecondName())<<";";
        in << QString::fromStdString(storage.at(i).getDateTime()->toString("dd.MM.yyyy/HH:mm").toStdString())<<";";
        in << QString::fromStdString(std::to_string(storage.at(i).getCost()))<<";";
        in << QString::fromStdString(std::to_string(storage.at(i).getWeight())) << endl;
    }
    MessageHelper::log("Saved count:"+std::to_string(storage.size()));
}

void DataBase::deleteById(int id)
{
    storage.erase(storage.begin()+id-1);
}

CustomerPackageEntity DataBase::getById(int id)
{
   return storage.at(id-1);
}
