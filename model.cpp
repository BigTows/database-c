#include "model.h"

Model::Model()
{

}

std::string Model::getString()
{
    bool flagBadString = false;
    std::string string;
    do{
        if (flagBadString){
            MessageHelper::log("Bad inserted String");
        }
        flagBadString=true;
        std::cin >> string;
    }while(string.length()<3);
    return string;
}

std::string Model::getName()
{
     bool flagBadName = false;
    std::string name;
    do{
        if (flagBadName){
            MessageHelper::log("Bad inserted Name");
        }
        name = Model::getString();
        flagBadName=true;
    }while(!Model::isCorrectName(name));
    return name;
}

float Model::getFloat()
{
    float result = 0;
    bool flagGoodFloat = true;
    do{
    flagGoodFloat = true;
    try{
        std::string input;
        std::cin >> input;
     result =  std::stof(input);
     if (result<=0){
         flagGoodFloat=false;
     }
    }catch(std::invalid_argument &e){
        flagGoodFloat = false;
    }
    }while(!flagGoodFloat);
    return result;
}

int Model::getInt()
{
    int result = 0;
    bool flagGoodFloat = true;
    do{
    flagGoodFloat = true;
    try{
        std::string input;
        std::cin >> input;
     result =  std::stoi(input);
     if (result<1){
         flagGoodFloat=false;
     }
    }catch(std::invalid_argument &e){
        flagGoodFloat = false;
    }
    }while(!flagGoodFloat);
    return result;
}


QDateTime * Model::getDate(){
    QString format = "dd.MM.yyyy/HH:mm";
    QDateTime * dateTime;
    do{
        MessageHelper::log("Write Date in format dd.MM.yyyy/HH:mm");
        QString dateString = QString::fromStdString(getString());
        dateTime = new QDateTime(QDateTime::fromString(dateString,format));
    }while(!dateTime->isValid());
   return dateTime;
}

bool  Model::isCorrectName(std::string name)
{
    bool badInput = true;
    for(unsigned long i = 0;i<name.length();i++){
        //std::cout <<name[i]<<" "<< (int)name[i] << ". ";
        if ((int)name[i]<65 || (int)name[i]>122 ||
                ((int)name[i]>91 && (int)name[i] < 97)) badInput = false;
    }
    return badInput;
}

Person * Model::getPersonByConsole()
{
    std::string name = Model::getName();
    std::string secondName = Model::getName();
 Person * person = new Person(&name,&secondName);
 return person;
}
