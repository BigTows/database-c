#ifndef CUSTOMERPACKAGEENTITY_H
#define CUSTOMERPACKAGEENTITY_H

#include <QDateTime>
#include "person.h"
#include <messagehelper.h>
class CustomerPackageEntity
{
private:
    Person * customer = 0;
    Person * sender= 0;
    QDateTime * dateTime= 0;
    double cost;
    double weight;
public:
    CustomerPackageEntity();
    QDateTime *getDateTime() const;
    void setDateTime(QDateTime *value);
    Person *getCustomer() const;
    void setCustomer(Person *value);
    Person *getSender() const;
    void setSender(Person *value);
    double getCost() const;
    void setCost(double value);
    double getWeight() const;
    void setWeight(double value);
    static CustomerPackageEntity * factory (QStringList CSV);
    void printData();
};

#endif // CUSTOMERPACKAGEENTITY_H
