#ifndef MODEL_H
#define MODEL_H

#include "person.h"
#include "messagehelper.h"
#include <string>
#include <iostream>
#include <QDateTime>
class Model
{
public:
    Model();
    static std::string getString();
    static std::string getName();
    static float getFloat();
    static int getInt();
    static QDateTime * getDate();
    static bool  isCorrectName(std::string name);
    static Person * getPersonByConsole();
};

#endif // MODEL_H
